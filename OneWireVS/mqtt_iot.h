/**
  ******************************************************************************
  * @file    mqtt_iot.h
  * @author  Pavel Nadein
  * @version V1.0
  * @date    17-March-2019
  * @brief   This file contains all the functions/macros to use mqtt protocol.
  ******************************************************************************
  */

#ifndef __MQTT_IOT_H__
#define __MQTT_IOT_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

typedef enum
{
	MQTT_MSG_RESERVED = 0x0,		//��������������
	MQTT_MSG_CONNECT = 0x1,		//������	�������	��	�����������	�	�������
	MQTT_MSG_CONNACK = 0x2,		//�������������	���������	�����������
	MQTT_MSG_PUBLISH = 0x3,		//����������	���������
	MQTT_MSG_PUBACK = 0x4,		//�������������	����������
	MQTT_MSG_PUBREC = 0x5,		//����������	��������
	MQTT_MSG_PUBREL = 0x6,		//����������	��	��������	���������
	MQTT_MSG_PUBCOMP = 0x7,		//����������	���������
	MQTT_MSG_SUBSCRIBE = 0x8,		//������	��	��������
	MQTT_MSG_SUBACK = 0x9,		//������	��	��������	������
	MQTT_MSG_UNSUBSCRIBE = 0xA,	//������	��	�������
	MQTT_MSG_UNSUBACK = 0xB,	//������	��	�������	������
	MQTT_MSG_PINGREQ = 0xC,	//PING	������
	MQTT_MSG_PINGRESP = 0xD,	//PING	�����
	MQTT_MSG_DISCONNECT = 0xE,	//���������	��	����������	��	�������
}mst_type_t;

typedef enum
{
	MQTT_SUCCESS = 0,
	MQTT_NOT_INIT,
	MQTT_INIT_ERROR,
	MQTT_AUTH_ERROR,
	MQTT_NOT_AUTH_ERROR,
	MQTT_PING_FAIL,
	MQTT_NO_MESSAGES,
	MQTT_FORMAT_ERR,
	MQTT_NO_MESSAGES_FOR_THIS_TOPIC,
}mqtt_err_t;

typedef struct
{
	uint8_t retain : 1;
	uint8_t qos : 2;
	uint8_t dup : 1;
	uint8_t msg_type : 4;
	uint8_t len;
	char data[];
}mqtt_fixed_header_t;

typedef struct
{
	uint8_t flags;
	uint8_t len;
	char data[];
}mqtt_flex_header_t;

class MQTT_Client
{
	public:
		/* Provide static buffer */
		MQTT_Client(int8_t (*_sendReceive)(char *buf, uint8_t size),
			char *_buffer, const char *_clientId, const char *_username,
			char *_password)
		{
			sendReceive = _sendReceive;
			buffer = _buffer;
			client_id = _clientId;
			username = _username;
			password = _password;
			get_password = NULL;
			is_auth = false;
			last_error = MQTT_NOT_INIT;
		};

		/* Dinamic memory allocation for buffer */
		MQTT_Client(int8_t (*_sendReceive)(char *buf, uint8_t size),
			const char *_clientId, const char *_username, const char *_password)
		{
			sendReceive = _sendReceive;
			buffer = NULL;
			client_id = _clientId;
			username = _username;
			password = _password;
			get_password = NULL;
			is_auth = false;
			last_error = MQTT_NOT_INIT;
		};

		/* Use external password function */
		MQTT_Client(int8_t(*_sendReceive)(char *buf, uint8_t size),
			char *_buffer, const char *_clientId, const char *_username,
			const char *(*_get_password)(const char *username))
		{
			sendReceive = _sendReceive;
			buffer = _buffer;
			client_id = _clientId;
			username = _username;
			password = NULL;
			get_password = _get_password;
			is_auth = false;
			last_error = MQTT_NOT_INIT;
		};

		/* This function used to transer buffer to MQTT server */
		int8_t (*sendReceive)(char *buf, uint8_t size);

		/* If you don't want to use malloc, use static buffer */
		char *buffer; //set to null if malloc used
		mqtt_err_t last_error;

		mqtt_err_t connect();
		mqtt_err_t ping();
		const char *get_last_error();
		bool authentificated();

	private:
		/* Credentials */
		const char *client_id, *username, *password;

		/* Get password fuction (optional) */
		const char *(*get_password)(const char *username);

		/* Authentification passed */
		bool is_auth;
};

class MQTT_Message
{
	public:
		MQTT_Message(MQTT_Client *_client)
		{
			client = _client;
		};

		mqtt_err_t send_message(const char * topic, const char * message);

	private:
		MQTT_Client *client;
};

#endif // !__MQTT_IOT_H__
