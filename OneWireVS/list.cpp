/**
  ******************************************************************************
  * @file    list.cpp
  * @author  Pavel Nadein
  * @version V1.0
  * @date    04-April-2019
  * @brief   This file contains list and dictonary API
  ******************************************************************************
  */

#include "list.h"
#include <math.h>

/* List will store an array of any data type */
struct list_t
{
	void *values;
	LIST_SIZE_TYPE size;
	list_t *next;
};

/* Dictonary will store a key value and an array of any data type */
struct dict_t
{
	char *key;
	void *values;
	LIST_SIZE_TYPE size;
	dict_t *next;
};

/* Get element by num or last one */
static struct list_t *get_list_element(LIST_SIZE_TYPE num, void *ptr)
{
	LIST_SIZE_TYPE i = 0;
	struct list_t *value = (struct list_t *)ptr;

	/* Roll around all elements in list */
	while (value->next && i++ != num)
		value = value->next;

	return value;
}

static struct list_t *get_last_list_element(void *ptr)
{
	return get_list_element((LIST_SIZE_TYPE)(-1), ptr);
}

/**
  * Add new element to list * 
  */
bool List::add(void *values, LIST_SIZE_TYPE size)
{
	/* Get last element of list to add a new one on top of it */
	struct list_t *ptr = get_last_list_element(list);

	/* Allocate memory for new element */
	struct list_t *new_value = ptr->values ? (struct list_t *)malloc(sizeof(list_t)) : ptr;

	/* Check that memory allocated for list element */
	if (!new_value)
		return false;

	/* Check that memory allocated for values of element */
	new_value->values = malloc(size);
	if (!new_value->values)
		return false;

	/* Save size */
	new_value->size = size;

	/* Copy values to list */
	memcpy(new_value->values, values, size);

	/* Attach new element on top of the list */
	ptr->next = new_value;

	/* Null terminate list */
	new_value->next = NULL;

	return true;
}

/**
  * Get elements count *
  */
LIST_SIZE_TYPE List::count()
{
	LIST_SIZE_TYPE i = 0;
	struct list_t *value = (struct list_t *)list;

	/* List is empty */
	if (!value->values)
		return 0;	
	
	/* Find null pointer incrementing counter */
	while (value) {
		value = value->next;
		i++;
	}

	return i;
}

/**
  * Get element by number *
  */
void * List::get(LIST_SIZE_TYPE num)
{
	struct list_t *ptr = get_list_element(num, list);

	return ptr->values;
}

/**
  * Remove element by number *
  */
bool List::remove_at(LIST_SIZE_TYPE num)
{
	struct list_t *ptr;

	LIST_SIZE_TYPE cnt = count();

	/* Check that value is in range */
	if (!cnt || num > cnt)
		return false;

	if (num)
	{
		ptr = get_list_element(num, list);
		struct list_t *next_ptr = ptr->next;

		/* Relese values memory */
		free(ptr->values);

		/* Relese memory of element */
		free(ptr);

		/* Take previous element */
		ptr = get_list_element(num - 1, list);

		/* Update link to next element */
		ptr->next = next_ptr;
	}
	else
	{
		/* Remove first element */
		ptr = (struct list_t *)list;

		/* Second element move to first */
		list = (void*)ptr->next;

		/* Relese values memory */
		free(ptr->values);

		/* Relese memory of element */
		free(ptr);
	}

	return true;
}

/**
  * Get element size by number *
  */
LIST_SIZE_TYPE List::get_element_size(LIST_SIZE_TYPE num)
{
	struct list_t *ptr = get_list_element(num, list);

	return ptr->size;
}

/**
  * Assuming that elements has 'int' type return maximum value *
  */
int List::get_max_int()
{
	typedef int rtn_type;
	struct list_t *element = (struct list_t *)list;
	rtn_type max = *(rtn_type*)element->values;

	/* Roll around all elements in list */
	while (element) {
		if (*(rtn_type*)element->values > max)
			max = *(rtn_type*)element->values;
		element = element->next;
	}

	return max;
}

/**
  * Assuming that elements has 'int' type return minimum value *
  */
int List::get_min_int()
{
	typedef int rtn_type;
	struct list_t *element = (struct list_t *)list;
	rtn_type min = *(rtn_type*)element->values;

	/* Roll around all elements in list */
	while (element) {
		if (*(rtn_type*)element->values < min)
			min = *(rtn_type*)element->values;
		element = element->next;
	}

	return min;
}

/**
  * Add new integer value to list *
  */
bool List::add_int(int value)
{
	return add(&value, sizeof(int));
}

/**
  * Assuming that elements has 'double' type return maximum value *
  */
double List::get_max_double()
{
	typedef double rtn_type;
	struct list_t *element = (struct list_t *)list;
	rtn_type max = *(rtn_type*)element->values;

	/* Roll around all elements in list */
	while (element) {
		if (*(rtn_type*)element->values > max)
			max = *(rtn_type*)element->values;
		element = element->next;
	}

	return max;
}

/**
  * Assuming that elements has 'double' type return minimum value *
  */
double List::get_min_double()
{
	typedef double rtn_type;
	struct list_t *element = (struct list_t *)list;
	rtn_type min = *(rtn_type*)element->values;

	/* Roll around all elements in list */
	while (element) {
		if (*(rtn_type*)element->values < min)
			min = *(rtn_type*)element->values;
		element = element->next;
	}

	return min;
}

/**
  * Assuming that elements has 'double' type return average value *
  */
double List::get_avg_double()
{
	typedef double rtn_type;
	struct list_t *element = (struct list_t *)list;
	rtn_type avg = 0;
	LIST_SIZE_TYPE i = 0;

	/* Roll around all elements in list */
	while (element) {
		avg += *(rtn_type*)element->values;
		element = element->next;
		i++;
	}

	return avg / i;
}

/**
  * Assuming that elements has 'double' type return root mean square value *
  */
double List::get_rms_double()
{
	typedef double rtn_type;
	struct list_t *element = (struct list_t *)list;
	rtn_type rms = 0;
	LIST_SIZE_TYPE i = 0;

	/* Roll around all elements in list */
	while (element) {
		rms += *(rtn_type*)element->values * *(rtn_type*)element->values;
		element = element->next;
		i++;
	}

	return sqrt(rms / i);
}

/**
  * Add new double value to list *
  */
bool List::add_double(double value)
{
	return add(&value, sizeof(double));
}

/**
  * Destructor *
  */
List::~List()
{
	struct list_t *element = (struct list_t *)list;

	/* Roll around all elements in list */
	while (element) {
		/* Release memory */
		free(element->values);
		free(element);

		/* Switch to next element */
		element = element->next;
	}
}

/* Get element by num or last one */
static struct dict_t *get_dict_element(LIST_SIZE_TYPE num, void *ptr)
{
	LIST_SIZE_TYPE i = 0;
	struct dict_t *value = (struct dict_t *)ptr;

	/* Roll around all elements in list */
	while (value->next && i++ != num)
		value = value->next;

	return value;
}

static struct dict_t *get_last_dict_element(void *ptr)
{
	return get_dict_element((LIST_SIZE_TYPE)(-1), ptr);
}

/**
  * Add new element to dictonary *
  */
bool Dict::add(const char *key, void *values, LIST_SIZE_TYPE size)
{
	/* Get last element of list to add a new one on top of it */
	struct dict_t *ptr = get_last_dict_element(dict);

	/* Allocate memory for new element */
	struct dict_t *new_value = ptr->values ? (struct dict_t *)malloc(sizeof(dict_t)) : ptr;

	/* Check that memory allocated for list element */
	if (!new_value)
		return false;

	/* Check that memory allocated for values of element */
	new_value->values = malloc(size);
	if (!new_value->values)
		return false;

	/* Save key */
	new_value->key = (char*)malloc(strlen(key) + 1);
	strcpy(new_value->key, key);

	/* Save size */
	new_value->size = size;

	/* Copy values to list */
	memcpy(new_value->values, values, size);

	/* Attach new element on top of the list */
	ptr->next = new_value;

	/* Null terminate list */
	new_value->next = NULL;

	return true;
}

/**
  * Get elements count *
  */
LIST_SIZE_TYPE Dict::count()
{
	LIST_SIZE_TYPE i = 0;
	struct dict_t *value = (struct dict_t *)dict;

	/* List is empty */
	if (!value->values)
		return 0;

	/* Find null pointer incrementing counter */
	while (value) {
		value = value->next;
		i++;
	}

	return i;
}

/**
  * Get key of element by number *
  */
const char * Dict::get_key(LIST_SIZE_TYPE num)
{
	struct dict_t *ptr = get_dict_element(num, dict);

	return (const char *)ptr->key;
}

/**
  * Get value of element by number *
  */
void * Dict::get_value(LIST_SIZE_TYPE num)
{
	struct dict_t *ptr = get_dict_element(num, dict);

	return ptr->values;
}

/**
  * Remove element by number *
  */
bool Dict::remove_at(LIST_SIZE_TYPE num)
{
	struct dict_t *ptr;

	LIST_SIZE_TYPE cnt = count();

	/* Check that value is in range */
	if (!cnt || num > cnt)
		return false;

	if (num)
	{
		ptr = get_dict_element(num, dict);
		struct dict_t *next_ptr = ptr->next;

		/* Relese key memory */
		free(ptr->key);

		/* Relese values memory */
		free(ptr->values);

		/* Relese memory of element */
		free(ptr);

		/* Take previous element */
		ptr = get_dict_element(num - 1, dict);

		/* Update link to next element */
		ptr->next = next_ptr;
	}
	else
	{
		/* Remove first element */
		ptr = (struct dict_t *)dict;

		/* Second element move to first */
		dict = (void*)ptr->next;

		/* Relese key memory */
		free(ptr->key);

		/* Relese values memory */
		free(ptr->values);

		/* Relese memory of element */
		free(ptr);
	}

	return true;
}

/**
  * Get element size by number *
  */
LIST_SIZE_TYPE Dict::get_element_size(LIST_SIZE_TYPE num)
{
	struct dict_t *ptr = get_dict_element(num, dict);

	return ptr->size;
}

/**
  * Find value of element by key *
  */
void * Dict::find_value(const char *key)
{
	struct dict_t *value = (struct dict_t *)dict;

	/* Roll around all elements in list */
	while (value) {
		if (strcmp(value->key, key) == 0)
			return value->values;

		value = value->next;
	}

	return NULL;
}

/* Destructor */
Dict::~Dict()
{
	struct dict_t *element = (struct dict_t *)dict;

	/* Roll around all elements in list */
	while (element) {
		/* Release memory */
		free(element->key);
		free(element->values);
		free(element);

		/* Switch to next element */
		element = element->next;
	}
}
