/**
  ******************************************************************************
  * @file    mqtt_iot.cpp
  * @author  Pavel Nadein
  * @version V1.0
  * @date    17-March-2019
  * @brief   This file contains all the functions/macros to use mqtt protocol.
  ******************************************************************************
  */

#include "mqtt_iot.h"
#include <string.h>

static mqtt_flex_header_t *fill_header(mqtt_flex_header_t *header, const char *data);

  /**
	* @brief  Connect to remote server
	* @retval SUCCESS or ERROR code
	*/
mqtt_err_t MQTT_Client::connect()
{
	const char mqtt_init_str[] = { 'M', 'Q', 'T', 'T', 0x04, 0xC2, 0x00, 0x3C };
	mqtt_err_t res;
	uint8_t buf_size;
	mqtt_fixed_header_t * mqtt_fixed_header;
	mqtt_flex_header_t  * mqtt_flex_header;
	char *buf;
	const char *psw = get_password ? get_password(username) : password;

	/* Check context */
	if (!client_id || !username || !psw)
		return MQTT_INIT_ERROR;

	/* Estimate buffer size */
	buf_size = (uint8_t)(sizeof(mqtt_init_str) + sizeof(mqtt_fixed_header_t) +
		4 * sizeof(mqtt_flex_header_t) +
		strlen(client_id) + strlen(username) + strlen(psw));

	/* Create a buffer for auth message */
	buf = buffer ? buffer : (char*)malloc(buf_size + sizeof(mqtt_fixed_header_t));

	/* Chech that buffer created */
	if (!buf)
		return MQTT_INIT_ERROR;

	/* Fixed header */
	mqtt_fixed_header = (mqtt_fixed_header_t*)buf;
	mqtt_fixed_header->msg_type = MQTT_MSG_CONNECT;
	mqtt_fixed_header->dup = 0;
	mqtt_fixed_header->qos = 0;
	mqtt_fixed_header->retain = 0;
	mqtt_fixed_header->len = buf_size - sizeof(mqtt_fixed_header_t);

	/* Init string #1 */
	mqtt_flex_header = (mqtt_flex_header_t*)mqtt_fixed_header->data;
	mqtt_flex_header->flags = 0;
	mqtt_flex_header->len = sizeof(mqtt_init_str);
	memcpy(mqtt_flex_header->data, mqtt_init_str, sizeof(mqtt_init_str));

	/* ClientId #2 */
	mqtt_flex_header = fill_header(mqtt_flex_header, client_id);

	/* Username #3 */
	mqtt_flex_header = fill_header(mqtt_flex_header, username);

	/* Password #4 */
	mqtt_flex_header = fill_header(mqtt_flex_header, psw);

	/* TODO: WTF! */
	buf[3] = 4;

	/* Connect to server and try auth */
	sendReceive(buf, mqtt_fixed_header->len + sizeof(mqtt_fixed_header_t));

	/* Return result */
	res = buf[0] == (MQTT_MSG_CONNACK << 4) ? MQTT_SUCCESS : MQTT_AUTH_ERROR;

	/* Release buffer if used */
	if (!buffer)
		free(buf);

	/* Save authentification result */
	is_auth = res == MQTT_SUCCESS;
	last_error = res;

	return res;
}

/**
  * @brief  Ping the remote server
  * @retval SUCCESS or MQTT_PING_FAIL
  */
mqtt_err_t MQTT_Client::ping()
{
	char buf[2] = { MQTT_MSG_PINGREQ << 4, 0x00 };

	sendReceive(buf, sizeof(buf));

	return buf[0] == (MQTT_MSG_PINGRESP << 4) ? MQTT_SUCCESS : MQTT_PING_FAIL;
}

const char *MQTT_Client::get_last_error()
{
	const char *error_list[] = {
		"Success",
		"Not initialized",
		"Init error",
		"Authentification error",
		"Not authentificated",
		"Ping fail",
		"No messages received",
		"Data format wrong",
		"No messages received for this topic"
	};

	return error_list[last_error];
}

bool MQTT_Client::authentificated()
{
	return is_auth;
}

/**
  * @brief  Send message to server
  * @retval SUCCESS or ERROR code
  * @param1 Topic name
  * @param2 Message text
  */
mqtt_err_t MQTT_Message::send_message(const char *topic, const char *message)
{
	mqtt_err_t res;
	mqtt_flex_header_t *mqtt_flex_header;

	/* Check that client is passed authentification */
	if (!client->authentificated())
		return MQTT_NOT_AUTH_ERROR;

	/* Estimate buffer size */
	uint8_t buf_size = (uint8_t)(sizeof(mqtt_fixed_header_t) +
		2 * sizeof(mqtt_flex_header_t) + strlen(topic) + strlen(message));

	/* Arrane the memory for message */
	char *buf = client->buffer ? client->buffer : (char*)malloc(buf_size);

	/* Fixed header for message */
	mqtt_fixed_header_t * mqtt_fixed_header = (mqtt_fixed_header_t*)buf;
	mqtt_fixed_header->msg_type = MQTT_MSG_PUBLISH;
	mqtt_fixed_header->dup = 0;
	mqtt_fixed_header->qos = 1;
	mqtt_fixed_header->retain = 0;
	mqtt_fixed_header->len = buf_size - sizeof(mqtt_fixed_header_t);

	/* Topic header */
	mqtt_flex_header = (mqtt_flex_header_t*)mqtt_fixed_header->data;
	mqtt_flex_header->flags = 0;
	mqtt_flex_header->len = strlen(topic);
	memcpy(mqtt_flex_header->data, topic, mqtt_flex_header->len);

	/* Message header */
	mqtt_flex_header = fill_header(mqtt_flex_header, message);

	/* Connect to server and try auth */
	client->sendReceive(buf, mqtt_fixed_header->len + sizeof(mqtt_fixed_header_t));

	res = buf[0] == (MQTT_MSG_PUBACK << 4) ? MQTT_SUCCESS : MQTT_AUTH_ERROR;

	/* Release buffer if used */
	if (!client->buffer)
		free(buf);

	return res;
}

static mqtt_flex_header_t *fill_header(mqtt_flex_header_t *header, const char *data)
{
	/* Move pointer to new position */
	header = (mqtt_flex_header_t*)(header->data + header->len);

	/* Flags always zero */
	header->flags = 0;

	/* Update lenght */
	header->len = strlen(data);

	/* Copy data */
	memcpy(header->data, data, header->len);

	/* Return updated header */
	return header;
}
