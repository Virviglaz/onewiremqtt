/**
  ******************************************************************************
  * @file    list.h
  * @author  Pavel Nadein
  * @version V1.0
  * @date    04-April-2019
  * @brief   This file contains list and dictonary API
  ******************************************************************************
  */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef LIST_SIZE_TYPE
#define LIST_SIZE_TYPE	uint32_t
#endif

class List
{
	public:
		/* Constructor */
		List()
		{
			const uint8_t size = 2 * sizeof(void*) + sizeof(LIST_SIZE_TYPE);
			list = malloc(size);
			memset(list, 0, size);
		};

		/* Destructor */
		~List();

		bool add(void *value, LIST_SIZE_TYPE size);
		LIST_SIZE_TYPE count();
		void * get(LIST_SIZE_TYPE num);
		bool remove_at(LIST_SIZE_TYPE num);
		LIST_SIZE_TYPE get_element_size(LIST_SIZE_TYPE num);

		/* list with integer values */
		int get_max_int();
		int get_min_int();
		bool add_int(int value);

		/* list with double values */
		double get_max_double();
		double get_min_double();
		double get_avg_double();
		double get_rms_double();
		bool add_double(double value);

	private:
		void *list;
};

class Dict
{
	public:
		/* Constructor */
		Dict()
		{
			const uint8_t size = 3 * sizeof(void*) + sizeof(LIST_SIZE_TYPE);
			dict = malloc(size);
			memset(dict, 0, size);
		}

		/* Destructor */
		~Dict();

		bool add(const char * key, void * value, LIST_SIZE_TYPE size);
		LIST_SIZE_TYPE count();
		const char *get_key(LIST_SIZE_TYPE num);
		void *get_value(LIST_SIZE_TYPE num);
		bool remove_at(LIST_SIZE_TYPE num);
		LIST_SIZE_TYPE get_element_size(LIST_SIZE_TYPE num);
		void * find_value(const char * key);

	private:
		void *dict;
};
