#include <stdio.h> //printf
#include <wiringPi.h> //gpio
#include <unistd.h> //delays
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/socket.h> //ethernet
#include <string.h> //strcpy
#include <netdb.h> //glibc
#include <arpa/inet.h>
#include "OneWireSensor.h"
#include "mqtt_iot.h"
#include "list.h"

#define ONEWIRE_PIN 		1
#define SERVER_NAME			"m11.cloudmqtt.com"
#define SERVER_PORT			"19220"
#define CLIENT_ID			"OrangePi"
#define USERNAME			"cxbvsnsa"
#define PASSWORD			"cOaAICGkMuX0"
#define TOPIC_NAME			"Temperature"
#define FILE_NAME			"pass.txt"

int mqtt_socket;
static char *get_ip(char *host);
static int get_data_from_file(char *param, char *value, 
	char const *default_value);
extern "C" int http_init(float *temp);

static void delay_func(uint16_t us)
{
	delayMicroseconds(us);
}

static void set_pin(uint16_t state)
{
	pinMode(ONEWIRE_PIN, OUTPUT);
	digitalWrite(ONEWIRE_PIN, state ? HIGH : LOW);
}

static uint16_t get_pin()
{
	pinMode(ONEWIRE_PIN, INPUT);
	return digitalRead(ONEWIRE_PIN);
}

static uint8_t crc8(uint8_t *buf, uint8_t size)
{
	uint8_t i, crc = 0;
	while (size--)
	{
		crc ^= *buf++;
		for (i = 0; i < 8; i++)
			crc = crc & 0x01 ? (crc >> 1) ^ 0x8C : crc >> 1;
	}
	return crc;
}

static int8_t tcp_sendReceive (char *buf, uint8_t size)
{
	uint8_t n;

	if (mqtt_socket < 0)
	{	
		printf("Error! Socket not created!\n");
		return 0;
	}

	send(mqtt_socket, buf, size, 0);

	uint16_t timeout_ms = 3000; //3s timeout

	do
	{
		 n = recv(mqtt_socket, buf, size, 0);
		 delayMicroseconds(1000);
	} while (--timeout_ms && !n);

	return 0;
}

int main(int argc, char* argv[])
{
	OneWireError err;
	char static_buffer[255], *server_ip;
	char server[20], port[10], client_id[20], 
		username[20], password[20], topic_name[20];

	/* Fetch credentials from file */
	get_data_from_file("server", server, SERVER_NAME);
	get_data_from_file("port", port, SERVER_PORT);
	get_data_from_file("client_id", client_id, CLIENT_ID);
	get_data_from_file("username", username, USERNAME);
	get_data_from_file("password", password, PASSWORD);
	get_data_from_file("topic_name", topic_name, TOPIC_NAME);

	/* Get ip by using dns query */
	printf("DNS for %s: %s\n", SERVER_NAME, server_ip = get_ip(server));
	if (server_ip == NULL)
		return -1; //dns fail

	/* Setup wiringPi library */
	wiringPiSetup();

	/* Get maximum prio for task */
	setpriority(PRIO_PROCESS, 0, -20);

	/* Print bullshit */
	printf("OneWire example\n");

	/* Create one wire interface */ 
	OneWire ow = OneWire(delay_func, set_pin, get_pin);

	/* Overrite default timing values */
	ow.high_pulse_wait_us = 50;
	ow.low_pulse_wait_us = 5;

	/* Create one sensor and attach it to interface */
	OneWireSensor sensor = OneWireSensor(&ow, RESOLUTION_12_BIT, crc8);

	/* Start converting data */
	err = sensor.start_conversion_skip_rom();
	printf("Start conv errcode: %s\n", sensor.error_desc(err));

	sleep(1);

	/* Read result from sensor */
	err = sensor.get_result_skip_rom();

	/* Printout the results */
	printf("Getting conv result: %s\n", sensor.error_desc(err));
	printf("Temperature: %.1f °C\n", sensor.temperature); 

	/* Create list of sensors referenced to existing one and connect to interface */
	OneWireSensors sensors = OneWireSensors(&ow, &sensor, 10);

	/* Search for devices over one wire bus */
	err = sensors.search_sensors();

	/* Connect to MQTT server */
	struct sockaddr_in mqtt_server;
	mqtt_server.sin_addr.s_addr = inet_addr(server_ip);
	mqtt_server.sin_family = AF_INET;
	mqtt_server.sin_port = htons(atoi(SERVER_PORT));
	mqtt_socket = socket(AF_INET, SOCK_STREAM, 0);

	if (connect(mqtt_socket, (struct sockaddr *)&mqtt_server, sizeof(mqtt_server)) < 0)
	{
		printf("Connection failed! Check network\n");
		mqtt_socket = -1;
	}

	bind(mqtt_socket, (struct sockaddr *)&mqtt_server, sizeof(mqtt_server));
	listen(mqtt_socket, 3);

	/* Create mqtt client */
	MQTT_Client client = MQTT_Client(tcp_sendReceive, static_buffer, 
		CLIENT_ID, USERNAME, PASSWORD);

	/* Connect client to server */
	client.connect();
	printf("MQTT connection result: %s\n", client.get_last_error());

	/* Create mqtt message handler */
	MQTT_Message mqtt_message = MQTT_Message(&client);

	/* Search sensors routine */
	printf("Search sensors passed with error code: %u, found sensros: %u\n",
		err, sensors.get_devices_found());

	/* Start http server in additional thread */
	printf("Starting web server: %s\n", http_init(&sensor.temperature) == 0 ? "Success" : "Fail");

	/* Create a list of values */
	List temp_list = List();

	/* For http testing we do only one sensor skipping rom access */
	while(1)
	{
		static char msg_buf[100];

		sleep(1);

		/* Start converting data */
		err = sensor.start_conversion_skip_rom();

		sleep(1);

		/* Read result from sensor */
		if (!err) {
			err = sensor.get_result_skip_rom();

			if (!err) {
				/* Add value to list */
				temp_list.add_double(sensor.temperature);

				if (temp_list.count() > 10)
					temp_list.remove_at(0);

				sprintf(msg_buf, "Temp: %.1f °C", sensor.temperature);

				mqtt_message.send_message(TOPIC_NAME, msg_buf);

				printf("Temperature: %.1f °C, resolution: %u, elements in list: %u, average value: %.1f\n",
					sensor.temperature, sensor.resolution, temp_list.count(), temp_list.get_avg_double());
			}
		}
	}


	/* Will not use for now */
	while (1)
	{
		/* Roll around found sensors */
		for (uint8_t i = 0; i != sensors.get_devices_found(); i++)
		{
			static char msg_buf[100];
			
			OneWireSensor *s = sensors.get_sensor(i);
			s-> resolution = RESOLUTION_12_BIT;

			printf("%2.2u: SN = ", i + 1);
			for (uint8_t j = 0; j != 8; j++)
				printf("%2.2X", s->sn[j]);
			printf("\n");

			err = s->start_conversion();
			delayMicroseconds(s->get_conv_time_ms() * 1000);

			err = s->get_result();
			sprintf(msg_buf, "Sensor %u: %.1f °C", i + 1, s->temperature);
			mqtt_message.send_message(TOPIC_NAME, msg_buf);
			printf("MQTT connection result: %s\n", client.get_last_error());

			/* Delay between measurements */
			sleep(5);

			if (err)
				break;
		}
	}

	printf("Error found, closing connection.\n");
	close(mqtt_socket);

	return -err;
}

static char *get_ip(char *host)
{
	struct hostent *he;
	struct in_addr **addr_list;
	int i;

	if ((he = gethostbyname(host)) == NULL)
		return NULL;

	addr_list = (struct in_addr **)he->h_addr_list;
	
	return (char*)inet_ntoa(*addr_list[0]);
}

static int get_data_from_file(char *param, char *value, 
	char const *default_value)
{
	FILE *file = fopen(FILE_NAME, "r");
	char str[50];

	/* Set default value */
	strcpy(value, default_value);

	/* Check file exist */
	if (file == NULL)
	{
		/* Create new file as template */
		file = fopen(FILE_NAME, "w");
		fprintf(file, "%s %s\n", "server", SERVER_NAME);
		fprintf(file, "%s %s\n", "port", SERVER_PORT);
		fprintf(file, "%s %s\n", "client_id", CLIENT_ID);
		fprintf(file, "%s %s\n", "username", USERNAME);
		fprintf(file, "%s %s\n", "password", PASSWORD);
		fprintf(file, "%s %s\n", "topic_name", TOPIC_NAME);
		fclose(file);

		/* Close and re-open for read */
		file = fopen(FILE_NAME, "r");
	}

	/* Look for string in file */
	while (fgets(str, sizeof(str), file))
	{
		/* format: 'login blablabla' */
		if (strncmp(str, param, strlen(param)) == 0)
		{
			char *pos;

			/* Copy content */
			strcpy(value, str + strlen(param) + 1);
			
			/* Remove \n at the end of string */
			if ((pos = strchr(value, '\n')) != NULL)
				*pos = '\0';
		}
	}

	fclose(file);

	return 0;
}
