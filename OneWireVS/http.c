#include <unistd.h> //delays
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>

float *temp_value = NULL;
static char *http_process(char *buf);

struct http_request
{
	char *init_text;
	char *(*handler)(char *buf);
};

/* To sort out incomming requests we should have a list with handlers */
const struct http_request request_list[] = {
	{ "dsfsdfs", http_process },
};

static void html_to_http(char *buf, char *header, char *body)
{
	/* Minimam header for http request to pass text data */
	const char std_header_Windows1251[] = "GET/HTTP/1.0 200 OK\r\nContent-Type: text/html; charset=Windows-1251\r\n\r\n";

	/* Format html header */
	sprintf(buf, "%s<html><head><title>%s</title></head><body>%s</body></html>", std_header_Windows1251, header, body);
}

static char *http_process(char *buf)
{
	char content[300];

	/* Put our data to html content */
	sprintf(content, "<h1>Last temperature: %.1f �C</h1><h2>Press F5 to refresh</h2>"
		"<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/266px-Tux.svg.png\">", temp_value ? *temp_value : 0);

	/* Format request using our data */
	html_to_http(buf, "Test page", content);

	return buf;
}

static void *http_connection_handler(void *socket)
{
	/* Data buffer for request */
	char buf[1500];
	ssize_t n;
	int conn_socket = *((int*)socket);

	do
	{
		/* If we receive the data, assuming it is http request */
		if (n = read(conn_socket, buf, sizeof(buf)) > 0)
			send(conn_socket, http_process(buf), strlen(buf), 0);

		/* Let OS breath */
		usleep(1000);

		/* Do while connection is alive */
	} while (getsockopt(conn_socket, SOL_SOCKET, SO_ERROR, NULL, NULL) == 0);

	/* Connection dropped, release socket */
	close(conn_socket);
}

static void *http_server(void *arg)
{
	/* This is address data to accept all incomming requests */
	struct sockaddr_in http_address;
	http_address.sin_family = AF_INET;
	http_address.sin_port = htons(80);
	http_address.sin_addr.s_addr = htonl(INADDR_ANY);
	
	/* We need to have socket opened to respond for incoming requests */
	int http_socket = socket(AF_INET, SOCK_STREAM, 0), err = 0;
	int connection_socket;

	/* Bind the socket to address */
	bind(http_socket, (struct sockaddr *)&http_address, sizeof(http_address));

	/* Accept incomming requests */
	listen(http_socket, 10);

	while (!err)
	{
		/* New connection comming, create a socket for it */
		connection_socket = accept(http_socket, (struct sockaddr*)NULL, NULL);

		/* To process the data we need to create a thread that will do that */
		if (connection_socket > 0) {
			pthread_t server_thread;
			err = pthread_create(&server_thread, NULL, &http_connection_handler, &connection_socket);
		}

		usleep(1000);
	}
}

int http_init(float *temp)
{
	pthread_t server_thread;

	/* Pointer where we gonna take our data */
	temp_value = temp;

	/* Create a thread which will listen ethernet for incoming requests */
	return pthread_create(&server_thread, NULL, &http_server, NULL);
	return 0;
}

//EXPORT_SYMBOL(http_init);